package com.afollestad.recyclicalsample

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.afollestad.recyclical.*
import com.afollestad.recyclicalsample.holders.*
import com.afollestad.recyclicalsample.model.*
import kotlinx.android.synthetic.main.activity_mutil_viewtype.*

class MutilViewtypeActivity : Activity() {

    var dataSource = dataSourceOf(
        FirstModel("item dau tien"),
        TwoModel("item thu 2"),
        ThreeModel("item thu 3")
    )


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mutil_viewtype)

        btn.setOnClickListener {
            dataSource.add(FourModel("item thu 4"))
        }

        recycle_multi.setup {
            withDataSource(dataSource)

            withItem<FirstModel>(R.layout.item_first) {
                onBind(::FirstHolder) { index, item ->

                    title.text = item.title
                    val dataSource = emptyDataSource()
                    dataSource.set(
                        IntArray(10) { it + 1 }
                            .map {
                                MyListItem(
                                    title = "#$it",
                                    body = "Hello, world! #$it"
                                )
                            }
                    )

                    recycler.setup {
                        withDataSource(dataSource)
                        withLayoutManager(
                            LinearLayoutManager(
                                this@MutilViewtypeActivity,
                                RecyclerView.HORIZONTAL,
                                false
                            )
                        )
                        withItem<MyListItem>(R.layout.my_list_item) {
                            onBind(::MyViewHolder) { index, item ->
                                icon.setImageResource(
                                    if (isSelectedAt(index)) {
                                        R.drawable.check_circle
                                    } else {
                                        R.drawable.person
                                    }
                                )

                                icon.setOnClickListener {
                                    //toast("Clicked $index: ${item.title} / ${item.body}")
                                    //dataSource.remove(item)
                                    dataSource.insert(
                                        0, MyListItem("xin chao", "hello")
                                    )
                                }

                                layout.setBackgroundColor(
                                    if (isSelectedAt(index)) {
                                        android.graphics.Color.RED
                                    } else {
                                        android.graphics.Color.WHITE
                                    }
                                )


                                title.text = item.title
                                body.text = item.body

                            }

                            onClick { index, item ->
                                nextScreen()
                                // if (index == position) {
//                                } else {
//                                    deselectAt(position)
//                                    toggleSelectionAt(index)
//                                }
//                                position = index
                            }

                            onLongClick { index, _ ->
                                toggleSelectionAt(index)
                            }
                        }
                    }
                }
            }
            withItem<TwoModel>(R.layout.item_two) {
                onBind(::TwoHolder) { index, item ->
                    title.text = item.title
                }
            }
            withItem<ThreeModel>(R.layout.item_three) {
                onBind(::ThreeHolder) { index, item ->
                    title.text = item.title
                }
            }
            withItem<FourModel>(R.layout.item_four) {
                onBind(::FourHolder) { index, item ->
                    title.text = item.title
                }
            }


        }

    }

    private fun nextScreen() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }


}
