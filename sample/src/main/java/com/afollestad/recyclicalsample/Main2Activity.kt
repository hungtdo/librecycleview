package com.afollestad.recyclicalsample

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.afollestad.recyclical.*
import com.afollestad.recyclicalsample.holders.MyViewHolder
import com.afollestad.recyclicalsample.model.MyListItem
import kotlinx.android.synthetic.main.activity_main2.*

class Main2Activity : AppCompatActivity() {
    private var toast: Toast? = null

    private val dataSource = emptySelectableDataSource().apply {

        onSelectionChange {

        }
    }

    private var b = dataSourceOf("a")

    var a = 0

    @SuppressLint("ResourceAsColor")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        btn.setOnClickListener {
            dataSource.set(
                IntArray(10) { it + 1 }
                    .map {
                        MyListItem(
                            title = "#$it",
                            body = "Hello, world! #$it"
                        )
                    }
            )

        }

        btn_add.setOnClickListener {
            dataSource.add(MyListItem("du lieu moi","new data"))
        }

        btn_sellectall.setOnClickListener {
            dataSource.selectAll()
        }
        btn_deselectall.setOnClickListener {
            dataSource.deselectAll()
        }


        recycle_test.setup {
            withEmptyView(txt_emty)
            withDataSource(dataSource)
            withItem<MyListItem>(R.layout.my_list_item) {
                onBind(::MyViewHolder) { index, item ->
                    icon.setImageResource(
                        if (isSelectedAt(index)) {
                            R.drawable.check_circle
                        } else {
                            R.drawable.person
                        }
                    )

                    icon.setOnClickListener {
                        toast("Clicked $index: ${item.title} / ${item.body}")
                        //dataSource.remove(item)
                        dataSource.insert(0, MyListItem("xin chao", "hello"))
                    }

                    btnDelete.setOnClickListener {
                        dataSource.remove(item)
                    }

                    layout.setBackgroundColor(
                        if (isSelectedAt(index)) {
                            Color.RED
                        } else {
                            Color.WHITE
                        }
                    )



                    title.text = item.title
                    body.text = item.body

                }

                onClick { index, item ->
                    if (index == position) {

                    } else {
                        deselectAt(position)
                        toggleSelectionAt(index)
                    }
                    position = index
                }

                onLongClick { index, _ ->
                    toggleSelectionAt(index)
                }
            }
        }
    }

    private var position = -1

    private fun toast(message: String) {
        toast?.cancel()
        toast = Toast.makeText(this, message, Toast.LENGTH_LONG)
            .apply { show() }
    }

}
